fn main() {

    let x: u64 =  45;  // data type i32 (int 32) by default
    let f: f64 =  6.7; // f32 by default
    let b: bool = false;

    println! ("{}\n{}\n{}", x, f, b)

    /*
     * i => signed int
     * u => unsigned int
     * f => float
     * b => bool
     */


}
