fn main () {
    let numbers: [i32; 5] = [1, 2, 3, 4, 5];

    println! ("{}", numbers[0]); // 1

    for i in numbers.iter() {
        println! ("{}", i);
    }

    let numbers = [2; 400];

    for i in 0..numbers.len() {
        println! ("{}", numbers[i]);
    }

}
