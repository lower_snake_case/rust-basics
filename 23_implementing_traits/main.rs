struct Person {
    name: String,
    age:  u8
}

impl ToString for Person {
    fn to_string (&self) -> String {
        return format! ("My name is {} and I am {}.", self.name, self.age);
    }
}

fn main () {
    // Trait is like an interface in java
    let jan = Person {name: String::from("Jan"), age: 22};

    println! ("{}", jan.to_string());
}
