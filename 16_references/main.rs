fn main () {
    let mut x     = 10;
    let xr        = &x;     // &    => immutable reference
    
    {
        let mut dom   = &mut x; // &mut => mutable reference

        *dom += 1;
    }

    println! ("x is {}", x);
}
