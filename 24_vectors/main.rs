fn main () {
    let     my_vec: Vec<i32> = Vec::new();
    let mut my_vec2 = vec![1, 2, 3, 4];

    println! ("{}", my_vec2[2]);

    my_vec2.push(49);
    my_vec2.remove(1); // by index

    for i in my_vec2.iter() {
        println! ("{}", i);
    }
}
