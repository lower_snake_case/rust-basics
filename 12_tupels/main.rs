fn main () {
    let tup1 = (20, "Rust", 3.14);
    let (a, b, c) = tup1;

    println! ("a is {}\nb is {}\nc is {}", a, b, c);
}
