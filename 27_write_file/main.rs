use std::fs::File;

use std::io::prelude::*;

fn main () {
     
    let mut file = File::create("output.txt")

        .expect("Couldn't create file!");

     
    file.write_all(b"Rust is great!\n123\n321") // b => byte slot
        .expect("Couldn't write to file.");

}
