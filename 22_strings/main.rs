fn main () {
    let mut my_string = String::from("How's it going?\nMy name is Jan");

    // Lenghth
    println! ("{}", my_string.len());

    // Is empty?
    println! ("{}", my_string.is_empty());

    for token in my_string.split_whitespace() {
        println! ("{}", token);
    }

    // Contains sub string?
    println! ("{}", my_string.contains("going"));

    // Append string
    my_string.push_str("\nNew String");

    println! ("{}", my_string);
}
