extern crate regex;
use regex::Regex;

fn main() {
    let re = Regex::new(r"(\w{5})").unwrap();

    let text = "dcod";

    println! ("found match? {}", re.is_match(text));

    match re.captures(text) {

        Some(caps) => println! ("Found match: {}", &caps[0]),
        None       => println! ("Could not find match!")
   
    }
}
