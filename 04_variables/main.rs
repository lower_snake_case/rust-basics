fn main () {
    
    let mut x = 45; /* 
                     * all variables are immutable by default
                     * use keyword 'mut' to make it mutable
                     */

    println! ("The value of x is {}", x); // passing x as a template

    x = 60;  // Redeclare x

    println! ("The value of x is {}", x);

}
