const MAXIMUM_NUMBER: u8 = 127;

fn main () {

    for i in 0..MAXIMUM_NUMBER + 1 {
        println! ("{}", i);
    }

}
