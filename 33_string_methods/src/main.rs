fn main() {
    
    /* Replace */
    {
        let my_string = String::from("Rust is fantastic!");

        println! ("After reolace {}", my_string.replace("fantastic", "great"));
    }


    /* Lines */
    {
        let my_string = String::from("The weather\nis nice\noutside mate!");

        for line in my_string.lines() {
            println! ("[  {}  ]", line);
        }
    }

    /* Split */
    {
        let my_string = String::from("Leave+a+like+if+you+enjoyed!");

        let tokens: Vec<&str> = my_string.split("+").collect(); // spilting by "+"

        println! ("{}", my_string);

        println! ("At index of two: {}", tokens[2]); // "like"

    }

    /* Trim */
    {
        let my_string = String::from("      My Name is Jan    \r\n");

        println! ("Before trim:\t{}", my_string);
        println! ("After trim:\t{}", my_string.trim());

    }

    /* Chars */
    {
        let my_string = String::from ("dcode on YouTube");

        println! ("{}", my_string);

        /* Get character at index */
        match my_string.chars().nth(4) { /* chars() => iterator; nth => at index */
            Some(c) => println! ("Character at index 4:\t{}", c), // if character was found
            None    => println! ("No character at index 4.")
        }

    }

}
