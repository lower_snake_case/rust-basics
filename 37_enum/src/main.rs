

fn main() {
    let name = String::from("Jan");

    println! ("Character at index 2: {}", match name.chars().nth(2) {
        Some (c) => c.to_string(),
        None     => "None".to_string()
    });

    println! ("Occupation is {}", match get_occupation("Jan") {
        Some (c) => c.to_string(),
        None     => "None".to_string()
    });
}

fn get_occupation (name: &str) -> Option<&str> {
    match name {
        "Jan"     => Some("Software Developer"),
        "Michael" => Some("Dentist"),
        _         => None
    }
}
