use std::process::Command;

fn main() {
    // python hello.py
    let mut command_runner = Command::new("python");

    command_runner.arg("target/hello.py");

    // Execute the command
    match command_runner.output() {
        Ok(o) => {
            unsafe {

                println! ("{}", String::from_utf8_unchecked(o.stdout));

            }

        }

        Err (e) => println! ("Error: {}", e)
    }
}
