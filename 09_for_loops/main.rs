fn main () {
    
    let animals = vec!["Rabbit", "Dog", "Cat"];
    
    for (id, value) in animals.iter().enumerate() { // vec.iter() => get iterator of vector
        println! ("the index is {} and the animal name is {}", id, value);
    }

}
