use std::io;

fn main () {
    
    let mut input = String::new();

    println! ("Input:");

    match io::stdin().read_line(&mut input) {

        Ok(_) => { // b => number of bytes?
            println! ("Your input was:\t{}", input.to_uppercase());
        },
        Err(e) => println! ("Error:\t{}", e)

    }

}
