use std::collections::HashMap;

fn main () {
    // hash map  => collection of key value pairs

    let mut marks = HashMap::new();

    // Add values
    marks.insert("Rust Programming", 96);
    marks.insert("Web Development",  94);
    marks.insert("UX Design",        75);
    marks.insert("Computer Science", 45);

    // Find length from HashMap
    println! ("{}", marks.len());
    
    // Get a single value
    match marks.get("Web Development") {

        Some(mark) => println! ("You passed {} for Web Dev!", mark),
        None       => println! ("You didn't study Web Development")
    }

    // Remove a pair
    marks.remove ("UX Design");

    // loop through HashMap
    for (subject, mark) in &marks {

        println! ("For {} you got {}%!", subject, mark);

    }

    // Check for value
    println! ("Did you study C++? {}", marks.contains_key("C++ Programming"));
}
