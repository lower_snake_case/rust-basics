mod dcode { /* mod => Module */
    
    fn chicken () { /* private */
        println! ("Chicken!");
    }
    
    pub fn print_message () {
        println! ("How's it going?");
        chicken();
    }

    pub mod water { /* nesting modules */
        
        pub fn print_message () {
            println! ("Water!");
        }

    }

}

fn main() {
    
    dcode::print_message();

    dcode::water::print_message();


}
