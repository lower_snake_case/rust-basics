fn main () {
    let number = 10;

    match number {
        1 => println! ("is one!"),
        2...20 => println! ("Is between two and twenty!"),
        21 | 22 => println! ("Is 21 or 22!"),
        _ => println! ("Doesn't match!")
    }
}
