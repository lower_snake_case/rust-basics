struct Rectangle {
    witdth: u8,
    height: u8
}

impl Rectangle {
    fn is_square (&self) -> bool {
        return self.witdth == self.height;
    }
}

fn main() {

}

fn give_two () -> i32 {
    return 2;
}

#[cfg(test)] // wouldn't be compiled until running `cargo test`
mod dcode_tests {
    #[test]

    #[should_panic] // test is expected to fail
    fn test_basic () {
        assert! (1 == 1); // `cargo test` to run tests

        panic! ("Oh no!"); // test will fail if it panics
    }

    #[test]
    // #[ignore] // ignore following test
    fn test_equals() {
        assert_eq! (super::give_two(), 1 + 1); // 2 == 1 + 1
        assert_ne! (super::give_two(), 2 + 1); // 2 != 2 + 1; ne => not equal
    }

    #[test]
    fn test_struct () {
        let r = super::Rectangle {
            witdth: 50,
            height: 25
        }; // not a square

        assert! (r.is_square() != true); // is not a square
    }
}
