fn main () {
    let mut x = 10;


    {
        let x = 15; // shadowing

        println! ("x is {}", x);
    }

    let x = "x is a string"; // changing type

    println! ("x is {}", x);

    let x = true;

    println! ("x is {}", x);
}
