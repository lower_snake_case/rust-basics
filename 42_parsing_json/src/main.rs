extern crate serde_json;

use serde_json::Value as JsonValue; // reference

fn main() {

    let json_string = r#"
        {
            "name": "Jan",
            "age": 22,
            "is_male": true
        }
    "#;

    let res = serde_json::from_str(json_string);

    if res.is_ok() { // json is valid

        let p: JsonValue = res.unwrap(); // json deserialized

        println! ("The name is {}", p["name"]);

    }
    else {
        println! ("Could not parse json.");
    }

}
