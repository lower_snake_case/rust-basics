# Rust



## The Rust Programming Language (Covers Rust 2018)

### [HTML version](https://doc.rust-lang.org/book/print.html#the-rust-programming-language)

### Offline version

#### Terminal

```bash
rustup docs --book
```

#### [Book](https://nostarch.com/Rust2018)

## Installation

- `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`



## Updates

- `rustup update`

> Rust installer and version management tool



## Package manager

> ### the Rust build tool and package manager
>
> When you install Rustup you’ll also get the latest stable version of the Rust build tool and package manager, also known as Cargo.

- create new project with `cargo new`
- build project with `cargo build`
- run project with `cargo run`
- test your project with `cargo test`
- build documentation for your project with `cargo doc`
- publish a library to [crates.io](https://crates.io) with `cargo publish`
- check version with `cargo --version`



## Compiler

> `rustc` is the compiler of the Rust programming language
>
>  Compilers take your source code and produce binary code, either as a library or executable

- `rustc main.rs`

## Crates

> Remember that a crate is a collection of Rust source code files [...]
>
> [@ Rust book](https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html#processing-a-guess)



> Note: You won’t just know which traits to use and which methods and functions to call from a crate. Instructions for using a crate are in each crate’s documentation. Another neat feature of Cargo is that you can run the `cargo doc --open` command, which will build documentation provided by all of your dependencies locally and open it in your browser. If you’re interested in other functionality in the `rand` crate, for example, run `cargo doc --open` and click `rand` in the sidebar on the left.

## Play list

[youtube](https://www.youtube.com/watch?v=vOMJlQ5B-M0&list=PLVvjrrRCBy2JSHf9tGxGKJ-bYAN_uDCUL)

## Missing pt. 38
### Http get request via reqwest crate
