extern crate rand;

use rand::Rng;

fn main() {
    let random_number = rand::thread_rng().gen_range(1, 11); // 1 - 10

    println! ("Random number:\t{}", random_number);

    // Flip a coin
    let random_bool = rand::thread_rng().gen_weighted_bool(2); // 1 of 2 chance for true

    println! ("Random boolean:\t{}", random_bool);
}
